# Zde budu zapisovat svůj postup formou deníku

* pročetl jsem si práce Jozefa Matouše, Adama Kollarčíka a Petra Brože, v nich jsem hledal informace ke konstrukci Acoumana
* zkontroloval jsem součástky a kabely, které jsem k Acoumanovi dostal
* prostudoval jsem si schémata PCB
* na gitu jsem pročetl návody na sestavení Acoumana (nejsou kompletní)
* ze schémat jsem zjistil, že napájení zesilovacího PCB má být 16V
* vytvořil jsem napájecí kabely na zesilovací shieldy, napájení na 16 V z laboratorního zdroje
* UART komunikace s FPGA zprovozněna
  * bylo třeba trochu pozměnit Quartus nastavení standalone/synchronized modu, pár muxů jsem přehodil, bay byla seriova komunikace brána z USB
* FPGA generuje, otestoval jsem, které piny mají jaké indexy s osciloskopem, u toho jsem odpálil jeden driver UCC27531, ten jsme potom odpájeli, je to na shieldu č. 0
* Raspberry PI
  * nainstaloval jsem potřebné moduly, stáhl git repozitáře
  * zprovoznil jsem displej, kameru
  * kamera funguje, jen na obrazu není nic vidět, když to není podsvícené ledkama
  * napájení ledkama už funguje (12V)
  * vision.py funguje, trackuje to kuličky, modrá se moc nedaří rozeznat, žlutá vůbec ne
* Simulink
  * nainstaloval jsem podpůrné resources pro simulink i Raspberry desku
  * povedlo se spustit Simulink v Monitor & Tune módu
  * vyvážil jsem podle vodováhy Transducers board
* Zapojení Acoumana
  * zkusil jsem několik variant zapojení a jedna byla správně, viz transducer_board.md
* přecházím na ransac detekci, snad bude fungovat líp
* povedlo se mi zkalibrovat kameru
* řízení kuliček funguje
* řízení triangles nefunguje, protože detekce trojúhelníků není dobrá, detekce pomocí MultiColorDetector detekuje trojúhelníky špatně a kalibrace nepomáhá, podobné jako problém s Balls. Zde bohužel řešení pomocí použití ransac Detectoru nefunguje, jelikož nemá ransac detektor detekci orientace.
* upravil jsem webové rozhraní tak, aby že jsem přidal mód Focal Point, kde se dá do Simulinku posílat pozice tlakového bodu. TODO: přidat nastavování tlaku pomocí kolečka myši.
* experimentoval jsem s hrubou moukou, viz experiments.md, výsledek není uspokojivý
* experimentoval jsem s dalšími sypkými materiály (hladká mouka, krupička, sůl, jemné piliny), nejlepšího výsledku jsem dosáhl i pilin, ale stále zůstává problém vytváření shluků, které dále nejdou rozdělit od sebe, levitují a nabalují na sebe další piliny
* připravil jsem hydrofobní průhlednou podložku a na ní provedl experimenty s kapičkou
* pokusy s kapičkami - na plastovém víčku od CD jsem roznesl několik různě velkých kapiček a zkoušel na ně působit akustickým tlakovým bodem v různých výškách a intenzitách, bohužel se mi nepovedlo je rozpohybovat v rovině XY. Ve směru Z se kapičky všemožně deformovaly, ale rozjet se do strany se jim nechtělo.
* zkoušel jsem další hydrofobní materiály: pečící papír měl nejlepší kontaktní úhel, ale ani na něm se nepodařilo pohnout s kapičkami
* zkoušel jsem míchat vodu a olej a to docela šlo, moc se olejové kapce nechtělo, ale podařilo se mi ji po chvíli rozehnat
* zjistil jsem, že když nastavím tři tlakové body do trojúhelníka se stěnou 1cm, tak začnou vznikat bubliny, když dám trojúhelník se stěnou 0.0075m a v pozici viz vlneni.png, vzniknou na hladine vlny a proudění -> míchání?
* nabarvil jsem vodovkami vodu, aby se dala lépe detekovat, ale výrazně se jí snížilo povrchové napětí, takže jsem to opustil a pokračuji jen s čistou vodou
* když k sobě přiblížím fokální body moc blízko, začne to být dost nepředvídatelné, někdy se sečtou a ale někdy je výsledek plochý
* zkusil jsem vytvořit "lžičku" ze tří fokálních bodů, ale výsledek závisel na umístění, občas bylo maaximum tlaku hezky zaoblené, ale občas byly body dál od sebe a občas zas blíž
* přidal jsem do raspi-ballpos detectoru detekci kapiček (DropDetector)
* řízení kapičky jakš takš funguje, nejepší pro m=1.54 a kapičku velkou cca 3jednotky
* objevil jsem nový *lepší* způsob řízení: tlakový bod se umístí nad kapičku, protlačí se do ní, a potom se posune směrem kam chceme kapičku posunout, posouvám ve vzdálenosti 2-6mm tlakový bod o velikosti 2900 Pa
* nové řízení funguje, pokud je kapička 3mm od referenčního bodu, tak vypínám její řízení
* vyrobil jsem demo pohybu tří kapiček zároveň - to nefunguje, je potřeba pohybovat s kapičkami jednotlivě postupně
* obarvil jsem kapičky barvivem, ale jdou teď velmi špatně detekovat
* jedna jednotka inzulinu je 0.01ml -> ideální velikost kapičky je 0.035ml, ta má asi 5mm v průměru
* pokusy se škrobem: škrob dost řídký na to, aby kapičky tekly po povrchu shora dolů, nedržel při sobě dost na to, aby se kapičky daly tlačit tlakovým bodem
  * zkusil jsem různé velikosti kapiček, ale žádné se nenechaly tlačit po povrchu
* foto kapky na: parafilmu, plastu, plexiskle, skle
* malá kapička se vypařila za hodinu - Acouman je u okna
* experimenty na hlaldině: pokud posouvám focal point 2800, tak to v některých místech začne rezonovat, to se děje jen u tlaku vyššího než 2300 - v RGBBalls používali tlak do této hodnoty
  * nepodařilo se mi vyfotit akustickou lžíci, pokud jsou body moc blízko sebe, tak klesá jejich velikost
* slučovat kapičky jde, ale pak je rozdělit už ne
