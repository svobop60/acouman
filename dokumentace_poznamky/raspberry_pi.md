# Raspberry Pi

V tomto souboru popíšu instalační proces programů do Raspberry Pi. Snahou bylo spustit program raspi-ballpos a webinterface.

* používám raspberry pi 3B+
* IP adresu lze zjistit pomocí příkazu *ifconfig* v terminalu po připojení SSH, pokud ji neznám, tak připojím k raspberry monitor a klávesnici a zjistím ji najetím myši vpravo nahoře na symbol síťe
* je potřeba napájet ze sítě stabilním napětím >= 5V, napájení z notebooku pomocí USB není stabilní a Raspberry se vypíná. Používám proto nabíječku vyrobenou přímo pro Raspberry. 
* Moje nastavení:
  * username: pi
  * password: a2c
  * IP: 147.32.86.178 (adresa na galerii)
* je potřeba nahrát operační systém (target) podporovaný Mathworks, aby šlo nahrávat a spouštět Simulinkové modely
* volné msíto na SD kartě se zjistí pomocí `df -h`, mně nestačila 8GB karta, musel jsem měnit za 32GB


## Instalace modulů

* potřeba nainstalovat vše podle <https://github.com/aa4cc/raspi-ballpos> a něco navíc:
  * nefungovala mi instalace: scipy a matplotlib, řešení je sudo apt-get update a potom  
  -> `sudo apt-get python3-matplotlib`  
  -> `sudo apt-get python3-scipy`
  * `sudo python3 -m pip install profilehooks screeninfo imutils flask_bootstrap flask_colorpicker`  
  *zde je potřeba dát sudo, aby bylo možné spustit program jako service!*
  * instalace cv2: je třeba nejprve nainstalovat Cmake, jinak se instalace nepovede!

  ```sh
  sudo apt update
  sudo apt full-upgrade
  pip3 install Cmake
  sudo pip3 install opencv-python
  ```

  * `sudo apt install python3-eventlet python3-flask`
  * `sudo pip3 install pyserial`
  * problémy s info-install vyřešeno pomocí `sudo mv /var/lib/dpkg/info/install-info.postinst /var/lib/dpkg/info/install-info.postinst.bad`
## Sestavení C modulů

Některé moduly spouštěné z pythonu jsou napsané v jazyce C a je třeba je nejprve sestavit, např. sharemem a multi_color_detector.

```sh
python3 setup.py build
sudo python3 setup.py install
```

## Úpravy programů


do vision.py bylo třeba na začátek importů dopsat
  
  ```py
  import matplotlib
  matplotlib.use('Agg')
  ```

  jinak vypisoval program raspi-ballpos error: `gdk_cursor_new_for_display: assertion 'GDK_IS_DISPLAY (display)' failed`

## Instalace starších verzí

přeinstaloval jsem na verze: (jinak byl problém se zpětnou kompatibilittou)

```sh
Flask-SocketIO==4.3.1
python-engineio==3.13.2  
python-socketio==4.6.0
  ```

## Řízení generátorů

Řízení generátoru se děje pomocí Matlab system objektu, ve kterém se volají funkce napsané v C v souboru <https://gitlab.fel.cvut.cz/aa4cc/acouman/simulink-controller\system_object\newGenerator\src\genControl.c>. Opět je potřeba ho sestavit pomocí

```sh
python3 setup.py build
sudo python3 setup.py install
```

V header souboru *genControl.h* je definovaný mimo jiné External Trigger, který spouští aktualizaci nastavení střídy a fáze výstupů.  
**External_trigger ... RPI_GPIO_P1_11**, což je na Raspberry Pi šestý pin odshora vlevo (USB je dole).

### Připojení generátorů k Raspberry

Generátory je třeba připojit přes USB konektor na zesilovacím Shieldu.  
Master je nastaven na /dev/ttyUSB1 a má číslo 1.  
Slave jsou na zbylých /dev/ttyUSB(0,2,3).  
Raspberry po restartu projde porty a připojí zařízení v daném pořadí, takže je nejlepší zapojit generátory následovně:  (schema raspberry USB portů)  
1|2  
3|0  
Zkontrolovat, zda jsou připojeny správně, můžeme pomocí
```sh
cd /dev
ls | grep USB
```

## Simulink

Pomocí tlačítka Hardware/Monitor&Tune je možné sledovat, co se děje v Raspberry za běhu modelu. Pokud chci program nahrát napevno, použiji záložku CCode/Build.  
Potřeba je nainstalovat extension Matlab Support for Raspberry Pi Hardware & Simulink Support for Raspberry Pi Hardware a Simulink Coder.  
Také je potřeba zajistit, aby byla cesta ke všem System Objektům v Path.
Pozor na nastavení single/double datových typů, všechno musí být single.
