# Různé poznámky, které nevím přesně kam zařadit

* FPGA a shield patří na sebe tak, že jsou USB na stejné straně
* omylem jsem při měření osciloskopem zkratoval driver U64 na shieldu 0, nahradili jsme jej driverem ze starší nepoužívané verze zesilovacího shieldu (jsou to piny nejvíc vpravo na GPIO SV4 u DC jacku)
* na aa4cc je prý nějaký specielní target pro raspberry na realtime, ten ale není potřeba, používám target dostupný od Mathworks
* s výškou vodní hladiny 1,2 cm funguje RGBBalls dobře, při nízké hladině není síla tlakového bodu dost velká na to, aby kuličku rozpohybovala
