# Deska akustických měničů

* měniče: Murata (G97) MA40S4S
  * max vstupní napětí 20 V_pp
  * vstupní signál: obdélník o frekvenci 300 kHz

## Jak zapojit generátory na desku měničů

Každý generátor má 4 výstupní porty: SV1, SV2, SV3 a SV4. Ty se připojí k desce akustických měničů pomocí plochých kabelů s 32 vodiči (viz [soupis součástí](dokumentace_soucasti.md)). Kabely jsou očíslovené zlomky, kde čitatel je číslo generátoru (0-4) a jmenovatel je číslo SV výstupu generátoru (1-4). Například kabel 1/3 je kabel, který připojuje výstup SV3 generátoru 1.

Každý generátor má na starosti jeden kvadrant desky akustických měničů. Kvadranty jsou rozděleny podle schématu:

![kvadranty generátorů](img/kvadranty_small.jpg)

Každý ze čtyř kabelů, které vystupují z generátoru, je rozdělen na dvě části s konektory velikosti 8x2 vodičů. Tyto konektory jsou očíslované čísly 1-16. Každému generátoru tedy přísluší 8 konektorů na straně desky měničů a čtyři konektory na straně generátoru. Číslování jde vzestupně, tedy například kabel 0/1 je zakončen konektory s čísly 1 a 2, kabel 2/3 je zakončen čísly 5 a 6.

Jak konektory zapojit k desce je znázorněno na schématu:

![zapojení konektorů](img/transducer_connect_small.jpg)

