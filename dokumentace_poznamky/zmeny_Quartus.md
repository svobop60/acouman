# Změny v Quartusu

V programu pro FPGA jsem provedl několik malých změn.

* prohodil jsem outputy Tristate Demux, byly: GPIO_00 u outWhen1
* prohodil jsem inputy UART nebo GPI0_28
* odebral jsem mutexování na základě chaining_mode
