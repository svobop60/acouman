# Praktické terminálové příkazy na Linuxu

(pomohl dr. Pavel Píša)

- odposlech portu 25001  
`nc -l -u -p 25001 | hexdump -e '/4 "%f " /4 "%f " /4 "%f " /4 "%f " /4 "%f " /4 "%f\n"'`

- zobrazit otevřené soubory:  
`lsof -i udp -l`  
`lsof -i tcp -n`

- zabít běžící program:  
`killall RGBballs.elf`

- zobrazit procesy, které mají v názvu RGB:  
`ps xa | grep RGB`

- přepnout uživatele do rootu, aby nebylo třeba před každým příkazem psát sudo  
`sudo -i`

- zobrazení statistiky o síťovém připojení  
`ss`
