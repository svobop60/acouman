
# Sestavení platformy

Platforma jako celek sestává z několika částí, které jsou popsány v následujících sekcích. Cílem sestavení platformy bylo dostat platformu do stavu, kdy je schopna řídit barevné kuličky na vodní hladině. Uznal jsem, že tehdy vše funguje tak jak má a je tedy možné započít s dalším výzkumem.

O každé dílčí součásti je v repozitáři separátní soubor s poznámkami, na který se lze skrze odkaz prokliknout, zde uvedu pouze základní fakta.

## [Raspberry Pi](raspberry_pi.md)

Mozkem systému je Raspberry Pi 3 B+. Na něm běží webové rozhraní, ke kterému je možné se připojit z prohlížeče v počítači a ovládat v něm pohyb objektů v manipulačním prostoru. Dále je možné v rozhraní spustit utilitu Kalibrace, ve které se anotací několika obrázků zkalibruje platforma (uloží výška desky akustických měničů, její natočení, atd.).

Zároveň na Raspberry běží Simulinkový model, který s webovým rozhraním komunikuje skrze UDP pakety. V nich si Simulinkový model posílá aktuální pozici kuliček a webové rozhraní posílá modelu referenční pozice. Simulinkový model lze z webového rozhraní spustit i vypnout.

Raspberry je připojeno k Ethernetu, mám k němu připojený monitor přes HDMI a čtyřmi USB jsou připojeny 4 generátory.  

## [Generátory](generator.md)

Generátory jsou tvořeny FPGA deskou DEO-Nano a zesilovacím shieldem (PCB), který výstup z FPGA zesiluje z 3.3V na 16V. Ke každému FPGA je na GPIO 2 připojeno synchronizační PCB, které navrhl Petr Brož, a které zajišťuje synchronizaci mezi FPGA.

Raspberry Pi je připojeno ke generátoru skrz USB mini konektor na zesilovacím shieldu, s FPGA je komunikace vedena skrze GPIO piny pomocí UART.  
USB do FPGA není potřeba vůbec zapojit, i napájení je zprostředkováno shieldem z USB.

## [Akustické měniče](akusticke_menice.md)

Akustické měniče jsou zapojeny do mřížky 64x64 a ke generátorům se připojují pomocí PCB desky.