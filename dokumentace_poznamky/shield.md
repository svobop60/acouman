# Zesilovací shieldy
Zde budu psát postup týkající se velkých zelených shieldů.
* prohlédl jsem si schéma a na vstupu je tam ochrana vstupního napětí pomocí LTC4365, ten integráč asi řeší přepěťové špičky.
* PHKD3NQ10T je FET tranzistor, jen buď pustí napětí dál, nebo ne, podle výstupu ochranného integráče
* UCC27531 jsou gate drivery ... zesiilují signál z IC
  * min input 10V, max input 32 V
* pin 12 v JP1 je připojen na VCC přes JP3, asi se tím dá napájet FPGA pod shieldem
* schéma big_shield.sch se od shield.sch liší v několika pinech na spodních portech, např pin 12 vůbec není obsazen
