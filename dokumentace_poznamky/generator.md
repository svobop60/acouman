# Generátor

Generátor sestává ze tří částí: FPGA, zesilovací shield a synchronizační shield.  
FPGA zesilovací shield je třeba zapojit na FPGA zeshora, správná orientace je taková, že jsou výstupy USB na stejné straně. Komunikace s Raspberry probíhá přes USB na shieldu, FPGA se napájí také ze shieldu. Potřeba je tedy připojit pouze USB a napájení 16 V na zesilovací shield a spojit synchoronizační shieldy + připojit je na jeden GPIO výstup Raspberry Pi (External Trigger).  
Synchronizační shield se zapojuje do FPGA zespodu.

|pohled zboku|pohled zepředu|
|:--:|:--:|
|![generátor zboku](img/generator_zboku.jpg)|![generátor zepředu](img/generator_zepredu.jpg)|d

## DEO-NANO FPGA

Program k němu je na: <https://github.com/aa4cc/fpga-generator>, editovat je ho možné v Quartus II 13.0sp1.

* Pro **trvalé nahrání** programu je potřeba .sof soubor konvertovat na .jic file podle [DEO-NANO User Manuálu](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&No=593&PartNo=4) (úplně dole). Jinak se program po odpojení smaže.
* Párkrát se mi stalo, že bylo potřeba program nahrát několikrát.

### DIP SWITCHES

Na FPGA desce jsou 4 přepínače, které mohou být buď ON nebo OFF, logická 1 je u čísel, log 0 je u nápisu ON.  
"Upper position" podle návodu na githubu je hodnota 0.

#### SWITCH 0

Nastavuje standalone/synchronized mode.  
0 ... standalone mode (LED_0 bliká)  
1 ... synchronized mode (LED_0 svítí)

#### SWITCH 1

Nastavuje zařízení roli master/slave. Master definuje pro zařízení slave CLK.

1 ... master  
0 ... slave

---

## Zesilovací shieldy

<https://gitlab.fel.cvut.cz/aa4cc/acouman/manufacturing-pcb/-/tree/master/Shield>

### Poznámky

* pracuji s verzí 1.2
* Napájení je zajišťováno přes DC_JACK na 16V, řeším to pomocí laboratorního zdroje, ze kterého mám vyvedeny 4 JACK konektory.
  
#### Součástky

Ze chématu desky jsem si něco zjistil k několika součástkám, abych si ověřil, že jim napětí 16V VDD neublíží.

* Na vstupu je ochrana vstupního napětí pomocí LTC4365, ten řeší přepěťové špičky.
* PHKD3NQ10T je FET tranzistor, jen buď pustí napětí dál, nebo ne, podle výstupu ochranného integráče.
* UCC27531 jsou gate drivery, zesilují signál z FPGA (3.3V -> 16V)
  * min input 10V, max input 32 V
* Pin 12 v JP1 je připojen na VCC přes JP3, tím se napájí FPGA pod shieldem.
* Schéma big_shield.sch se od shield.sch liší v několika pinech na spodních portech, např pin 12 vůbec není obsazen, board je pak velmi jiný.  
**big_shield je starší verze!** (v0.9)

### Výstupy GPIO

indexování výstupů na generátoru (každý generátor má 64 výstupů)

"*vpravo*" znamená "*na straně, kde je DC Jack*" a "*vlevo*" je na druhé straně.

0-15  ... SV4, 0 vpravo  
16-31 ... SV3, 16 vlevo  
32-47 ... SV2, 32 vlevo  
48-63 ... SV1, 48 vpravo  

---

## Synchronizační Shield

* Vytvořil je Petr Brož
* Master slave systém
* Je potřeba je spojit plochým kabelem se šesti vodiči skrze SV konektor.
* Koaxiálními kabely je veden CLK signál z mastera, i master přijímá CLK po koaxiálním kabelu, aby byla délka vedení stejná pro všechna 4 zařízení.
* Více viz <https://github.com/aa4cc/fpga-generator/tree/master/chaining_shield>.

![synchronizační shield](img/synch_indexace2.png)

### Piny J0

Popis, kam jsou propojeny komunikační piny konektoru J0, kterým se synchronizační shield připojuje k FPGA.

Indexuje se zprava, pin 1 je vpravo nahoře, pod ním je pin 2, pokračování je naznačeno na obrázku.

|PIN J0|synch. shield|FPGA|
|-----:|:-----------:|:---|
| 2 | S | GPIO_2_IN0 (CLK_IN)|
| 6 | M4| GPIO_2_1  (CLK_OUT_1)|
|10 | M3| GPIO_2_5  (CLK_OUT_2)|  
|12 | M2| GPIO_2_7  (CLK_OUT_3)|  
|16 | M1| GPIO_2_11 (CLK_OUT_4)|
|11 | SV_5| GPIO_2_6  (output UART)|
|13 | SV_6| GPIO_2_8  (input  UART)|  
|15 | SV_2| GPIO_2_10 (BIDIR_RESET)|  
|17 | SV_4| GPIO_2_12 (Trigger External)|

### Piny SV

Indexace pinů na konektoru, kterým se propojují synchronizační shieldy mezi sebou, je vidět na obrázku.

SV_1 a SV_3 jsou připojeny na GND.

### LED

Na synchronizačním shieldu je LED dioda, která signalizuje aktuální stav nastavení FPGA. Zobrazuje stejnou hodnotu jako LED na FPGA a je připojena na FPGA pin GPIO_2_4 (output chaining master LED).
