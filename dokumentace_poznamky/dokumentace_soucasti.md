# Soupis dílů platformy Acouman

## Šedivé ploché kabely

Je potřeba 16 plochých kabelů s alespoň 32 vodiči. Využity jsou kabely s 34 vodiči, přičemž 2 vodiče jsou nechány nepřipojeny.
Na na jedné straně je kabel zakončen konektorem FC-34P se 17x2 vstupy.  
Na druhé straně je kabel zakončen dvěma konektory FC-16P s 8x2 výstupy.

Kompilace tří částí kabelu je vidět na obrázku:
![plochý kabel](img/Cable.png)

## Deska akustických měničů

K měničů vede 16 vstupů na dvou stranách, tedy 32 celkem, každý je pro konektor FC-16P.  
Označeny jsou CH0-CH63.  
Měničů je na desku připojeno v mřížce 16*16 = 256

Schéma desky je k nalezení na <https://gitlab.fel.cvut.cz/aa4cc/acouman/manufacturing-pcb/-/tree/master/TransducerBoard>.

## Zesilovací shieldy (Shield v1.2)

Zesilují výstup generátorů signálu na 16 $\text{V}_{\text{pp}}$. Návrh viz <https://gitlab.fel.cvut.cz/aa4cc/acouman/manufacturing-pcb/-/tree/master/Shield>.  
K dispozici jsou 4 zesilovací shieldy verze 1.2, které jsou označeny čísly 0-3.  

využité konektory:
- Mini-USB, DC-Jack na 16V.
- 4 výstupy pro kabely 16x2 nahoře.
- 2 vstupy 20x2 dole (lze spojit s modrým DEO-NANO). 

| pohled shora|pohled zespoda|
|:--:|:--:| 
|![FPGA zeshora](img/amplify_shield_up.jpg)|![FPGA zespoda](img/amplify_shield_down.jpg)|


## Synchronizační shieldy

Zajišťují synchronizaci času změny fáze výstupu generátorů. Návrh viz <https://github.com/aa4cc/fpga-generator/tree/master/chaining_shield>.

Jsou potřeba propojit plochým kabelem s šesti vodiči, přes který spolu mohou komunikovat. Zároveň je ke kabelu potřeba připojit i GPIO výstup Raspberry Pi, které tak distribuuje signál TRIGGER_EXT, jehož náběžná hrana značí okamžik změny fáze generátorů.  
Bílý koaxiální kabel rozvádí signál CLK mezi generátory z generátoru master do všech čtyř. Je zapojen i sám do sebe, aby se eliminovalo zpoždění.  
Více o propojení v sekci [generátor](generator.md).


![alt](img/synchro_shield2.jpg)

## FPGA DEO-NANO
Základní stavební blok generátoru, zde běží program, který generuje fázově 64 fázově posunutýc signálů.  
Každé má dva 20x2 pinů na horní straně a 13x2 pinů dolní.  

| pohled shora|pohled zespoda|
|:--:|:--:| 
|![alt](img/FPGA_up.jpg)|![alt](img/FPGA_down.jpg)|


### Velké FPGA
Toto bylo FPGA pro první iteraci platformy, pro aktuální není potřeba.  
![alt](img/FPGA_big2.jpg)

## Raspberry Pi 3B+

Centrální jednotka je Raspberry Pi, model 3 B+.

![raspberry pi](img/raspberry2.jpg)
