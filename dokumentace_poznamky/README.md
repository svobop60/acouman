# Acouman - Ultrazvuková manipulace s kapalinou a sypkými materiály

Tato dokumentace vznikla jako součást bakalářské práce na ČVUT FEL v Praze.  
Bakalářskou práci je možno nalézt v repozitáři [svobop60/acouman/bakalarka](https://gitlab.fel.cvut.cz/svobop60/acouman/-/tree/main/bakalarka).

## Obsah

1. [Poznámky k sestavení zařízení Acouman](sestaveni.md)
2. [Dokumentace k jednotlivým součástem zařízení](dokumentace_soucasti.md)

## Krátký popis zařízení

Acouman je platforma pro akustickou manipulaci. Pomocí fázové modulace a interference zvuku z několika akustických měničů se vytváří tlakové pole, pomocí něhož je možné působit silou na objekty. Zároveň je možné sledovat pozici objektů pomocí kamery a zavedením zpětnovazební smyčky tak řídit pozici objektů.

## Úkol projektu

Úkolem tohoto bakalářského projektu bylo znovu sestavit platformu Acouman, která byla několik let v rozebraném stavu.  
Platformu v rámci bakalářských prací vytvořili Josef Matouš a Adam Kollarčík. Spolu vytvořili první iteraci platformy, ve které bylo pouze 64 akustických měničů. Na to navázal svou diplomovou prací Josef Matouš, ve které rozšířil zařízení na 256 akustických měničů a zvětšil tak i manipulační prostor, ve kterém se mohou objekty pohybovat.

## Ultrazvuková manipulace s kapalinou a sypkými materiály

Po zprovoznění platformy bylo úkolem provést experimenty se sypkými materiály a kapalinou a prozkoumat možnosti platformy v této oblasti. Konkrétně jsem se snažil o řízený pohyb kapiček vody, míchání dvou kapalin a vytváření obrazců v sypkém materiálu. Zpracovaný pospis experimentů a jejich výsledky jsou k nalezení v textu bakalářské práce.

